var questionsSet = [
    
    {
        question: "The poor quality of selection will mean extra cost on _______ and supervision.?",
        answers: {
            a: "Training",
            b: "Recruitment",
            c: 'Work quality'
        },
        correctAnswer: 'b'
    },
    {
        question: "Which one is correct team name in NBA?",
        answers: {
            a: "New York Bulls",
            b: "Los Angeles Kings",
            c: "Huston Rocket"
        },
        correctAnswer: "c"
    },

    {
        question: "Who is the Line Manager Of PowerTech?",
        answers: {
            a: "Abayomi",
            b: "Philip",
            c: "Sinmi"
        },
        correctAnswer: "c"
    },
    {
        question: "Which of the following act deals with recruitment and selection?",
        answers: {
            a: ' Child labour act',
            b: 'The apprentices act',
            c: 'Mines act'
        },
        correctAnswer: "c"
    },
    {
        question: "Who is the strongest?",
        answers: {
          a: "Superman",
          b: "The Terminator",
          c: "Waluigi, obviously"
        },
        correctAnswer: "c"
      },
      {
        question: "What is the best site ever created?",
        answers: {
          a: "SitePoint",
          b: "Simple Steps Code",
          c: "Trick question; they're both the best"
        },
        correctAnswer: "c"
      },
      {
        question: "Where is Waldo really?",
        answers: {
          a: "Antarctica",
          b: "Exploring the Pacific Ocean",
          c: "Sitting in a tree",
          d: "Minding his own business, so stop asking"
        },
        correctAnswer: "d"
      }
];