
let currentPageDisplay = document.querySelector(".currentPage");
let totalPagesDisplay = document.querySelector(".totalPages");
let paginationDisplay = document.querySelector(".pagination");
let Pagesranges = document.querySelector(".Pagesranges");
let Spinner = document.querySelector(".spinner");

let listingTable = document.querySelector(".blogRow");

function postPage() {
  Spinner.style.display = "block";
  fetch("https://jsonplaceholder.typicode.com/posts?_start=0&_limit=20")
    .then(response => response.json())
    .then(data => {
      Spinner.style.display = "none";
      (function() {
        document.getElementById("first").addEventListener("click", firstPage);
        document.getElementById("next").addEventListener("click", nextPage);
        document
          .getElementById("previous")
          .addEventListener("click", previousPage);
        document.getElementById("last").addEventListener("click", lastPage);


          var postUsers = {};
        
          data.forEach(user => {
            postUsers[user.postUsersId] = user;
          });
        
          var res = ExternalArr.map(function(sub) {
            var user = postUsers[sub.postUsersId];
            if (user) {
              for (var key in user) {
                sub[key] = user[key];
              }
            }
            return sub;
          });
        

      

        let listPage = res;
        let pageList = [];
        let currentPage = 1;
        let numberPerPage = 5;
        let numberOfPages = 0;

        function scrollTop() {
          window.scroll({
            top: 0,
            left: 0,
            behavior: "smooth"
          });
        }

        function makeList() {
          numberOfPages = getNumberOfPages();
        }

        function getNumberOfPages() {
          return Math.ceil(listPage.length / numberPerPage);
        }

        function nextPage() {
          currentPage += 1;
          loadList();
          scrollTop()
        }

        function previousPage() {
          currentPage -= 1;
          loadList();
          scrollTop()
        }

        function firstPage() {
          currentPage = 1;
          loadList();
          scrollTop()
        }

        function lastPage() {
          currentPage = numberOfPages;
          loadList();
          scrollTop()
        }

        function loadList() {
          var begin = (currentPage - 1) * numberPerPage;
          var end = begin + numberPerPage;

          pageList = listPage.slice(begin, end);
          currentPageDisplay.innerHTML = currentPage;

          pagination();
          check();
        }

        totalPagesDisplay.innerHTML = listPage ? Math.ceil(listPage.length / numberPerPage) : 7;

        function pagination() {
          listingTable.innerHTML = "";
          for (var i = 0; i < pageList.length; i++) {
            listingTable.innerHTML +=  `
            <div class ="col-lg-12 text-center pt-5" data-aos="fade-up" data-aos-duration="1000" style={{width:'auto'}}>
            <h2 class='pl-5 ml-5 py-3 text-left'>${pageList[i].headerText || 'Maria'}</h2>
            <div>
            <img src =${
              pageList[i].img
            } alt ="img" data-key='${i}' class = "blog_imageTop center"/>
                  </div>  
                  <div class='row'>
                  
                  <div class="col-lg-1 text-left">
                  <img src =${
                    pageList[i].author
                  } alt ="img" data-key='${i}' class = "blog_image"/>
                  </div>
                <div class="col-lg-10 pt-5 text-left">
                <h4>${pageList[i].header}</h3>
                  <p>${pageList[i].commentParagraph + pageList[i].body}</p>
                  
                  <div class = "commenting py-4">
                  
                      <div>                    
                      <span class= "font-weight-bold thumbs"><em>${pageList[i].hrs +
                        " ago"}</em></span>
                        <span>${pageList[i].read + "s" + " read"}</span>
                        <ul class = "postLikes mt-3">
                          <li><a href = "">${"Reply"}</a></li>
                          <li><a href = "">Share</a></li>
                          <li >${pageList[i].likes + " likes"} </li>
                          <li class="thumbs">${pageList[i].thumbsUp} </li>
                          <li class="thumbs">${pageList[i].thumbsDown} </li>
                          </ul>
                      </div>
                      
                      <div>
                      
                      <a  href="../postComment.html?postId=${
                        pageList[i].id
                      }" class="aTagPost">View Comment</a>
                      </div>
                      </div>
                      
                      </div>
                      
                      </div>
              
              
                      
              </div>
              
        `;
          };
        }

        function check() {
          document.getElementById("next").disabled =
            currentPage == numberOfPages ? true : false;
          document.getElementById("previous").disabled =
            currentPage == 1 ? true : false;
          document.getElementById("first").disabled =
            currentPage == 1 ? true : false;
          document.getElementById("last").disabled =
            currentPage == numberOfPages ? true : false;
        }

        function load() {
          makeList();
          loadList();
        }

        window.onload = load();
      })();
    })
    .catch(err => {
      paginationDisplay.style.display = "none";
      Pagesranges.style.display = "none";
      Spinner.style.display = "none";
      document.querySelector(".blogError").innerHTML =
        "Check Your Internet Connection";
    });
}

postPage();









