


let ExternalArr = [
  {
    id: 1,
    hrs: "2hrs",
    author:'../images/people-1690965_640.jpg',
    headerText: "Iphone iX6.",
    header: "Steven Paul Jobs",
    read :"7min",
    commentParagraph:
      `Founded in a garage in 1976 by Steve Jobs, Steve Wozniak, and Ronald Wayne, Apple began as a personal computer pioneer that today makes everything from laptops to portable media players. Headquartered in Cupertino, California, the consumer electronics giant entered the smartphone market with the iPhone in 2007, and the tablet market with the iPad in 2010, and the smartwatch market with the Apple Watch in 2014. Apple's latest mobile launch is the iPhone 11 Pro. The smartphone was launched in 10th September 2019. The phone comes with a 5.80-inch touchscreen display with a resolution of 1125 pixels by 2436 pixels at a PPI of 458 pixels per inch. <br/>

      The iPhone 11 Pro is powered by hexa-core Apple A13 Bionic processor and it comes with 4GB of RAM. The phone packs 64GB of internal storage cannot be expanded. As far as the cameras are concerned, the iPhone 11 Pro packs a 12-megapixel (f/1.8) + 12-megapixel (f/2.4) + 12-megapixel (f/2.0) primary camera on the rear and a 12-megapixel front shooter for selfies.
      
      The iPhone 11 Pro runs iOS 13 and is powered by a 3046mAh non removable battery. It measures 144.00 x 71.40 x 8.10 (height x width x thickness) and weigh 188.00 grams.<br/>
      
      The iPhone 11 Pro is a dual SIM (GSM and GSM) smartphone that accepts Nano-SIM and eSIM. Connectivity options include Wi-Fi, Wi-Fi standards supported, GPS, Bluetooth, NFC, Lightning, 3G and 4G (with support for Band 40 used by some LTE networks in India). Sensors on the phone include Face unlock, 3D face recognition, Compass Magnetometer, Proximity sensor, Accelerometer, Ambient light sensor, Gyroscope and Barometer.`,
    likes: 7,
    icons: `<i class="fas fa-shower fa-5x"></i>`,
    img:
      "../images/smartphone-1894723_1280.jpg",
    thumbsUp: '<i class="fas fa-thumbs-up" style="color:green;"></i>',
    thumbsDown: '<i class="fas fa-thumbs-down" style="color:red;"></i>',
    handShake: `<i class="fas fa-handshake fa-2x"></i>`
  },
  {
    id: 2,
    hrs: "5hrs",
    headerText: "Progressive  Web Application",
    header: "Haskey Gray",
    author:'../images/man-852766_640.jpg',
    read :"5min",

    commentParagraph:` <strong> PWA (Progressive Web Apps) </strong> development is a cheaper and more effective alternative to separate native apps for iOS, Android and the Web. One codebase instead of three—you do the math. There is no bigger and more open platform than the Web and, given over eight years of experience, we know all its ins and outs. Therefore, if you can't make a decision or need to brainstorm your idea, we will use our expertise from previous projects to help you achieve your goals. <br/>
    Progressive Web Apps are installable and live on the user's home screen, without the need for an app store. They offer an immersive full screen experience with help from a web app manifest file and can even re-engage users with web push notifications. <br/>

The Web App Manifest allows you to control how your app appears and how it's launched. You can specify home screen icons, the page to load when the app is launched, screen orientation, and even whether or not to show the browser chrome. <br/>Lighthouse, an open-source, automated tool for improving the quality of your Progressive Web Apps, eliminates much of the manual testing that was previously required. You can even use Lighthouse in continuous integration systems to catch regressions.
    `,
    likes: 5,
    icons: `<i class="fab fa-apple fa-5x" style="color:green;"></i>`,
    img:
      "../images/imac-1999636_1280.png",
    thumbsUp: '<i class="fas fa-thumbs-up" style="color:green;"></i>',
    thumbsDown: '<i class="fas fa-thumbs-down" style="color:red;"></i>',
    handShake: `<i class="fas fa-handshake fa-2x"></i>`
  },
  {
    id: 3,
    hrs: "9hrs",
    headerText: "Google",
    header: " Sergey Brin and Larry Page ",
    read :"17min",
    author:'../images/newpeople.jpg',
    commentParagraph:`<strong>Google Inc. </strong>, American search engine company, founded in 1998 by Sergey Brin and Larry Page that is a subsidiary of the holding company Alphabet Inc. More than 70 percent of worldwide online search requests are handled by Google, placing it at the heart of most Internet users’ experience. Its headquarters are in Mountain View, California. <br/>

    Google began as an online search firm, but it now offers more than 50 Internet services and products, from e-mail and online document creation to software for mobile phones and tablet computers. In addition, its 2012 acquisition of Motorola Mobility put it in the position to sell hardware in the form of mobile phones.
    <br/>
     Google’s broad product portfolio and size make it one of the top four influential companies in the high-tech marketplace, along with Apple, IBM, and Microsoft. Despite this myriad of products, its original search tool remains the core of its success. In 2016 Alphabet earned nearly all of its revenue from Google advertising based on users’ search requests.
     <br/>
     To accommodate this unprecedented mass of data, Google built 11 data centres around the world, each of them containing several hundred thousand servers (basically, multiprocessor personal computers and hard drives mounted in specially constructed racks). Google’s interlinked computers probably number several million. The heart of Google’s operation, however, is built around three proprietary pieces of computer code: Google File System (GFS), Bigtable, and MapReduce. GFS handles the storage of data in “chunks” across several machines; Bigtable is the company’s database program; and MapReduce is used by Google to generate higher-level data (e.g., putting together an index of Web pages that contain the words “Chicago,” “theatre,” and “participatory”).
     `,
    likes: 4,
    icons: `<i class="fas fa-lemon fa-5x" style="color:orange;"></i>`,
    img:
      "../images/bar-621033_1920.jpg",
    thumbsUp: '<i class="fas fa-thumbs-up" style="color:green;"></i>',
    thumbsDown: '<i class="fas fa-thumbs-down" style="color:red;" ></i>',
    handShake: `<i class="fas fa-handshake fa-2x"></i>`
  },
  {
    id: 4,
    hrs: "7hrs",
    headerText: "Apple Watch",
    header: "Gaskey Fish",
    author:'../images/portrait-3353699_640.jpg',

    read :"9min",

    commentParagraph:`The Apple Watch is a line of smartwatches designed, developed, marketed and sold by Apple Inc. It incorporates fitness tracking and health-oriented capabilities with integration with iOS and other Apple products and services. <br/> The goal of the Apple Watch was to complement an iPhone and add some new functions. Kevin Lynch was hired by Apple to make wearable technology for the wrist. He said: "People are carrying their phones with them and looking at the screen so much. People want that level of engagement. <br/> But how do we provide it in a way that's a little more human, a little more in the moment when you’re with somebody?" Apple's development process was held under wraps until a Wired article revealed how some internal design decisions were made <br/>`,
    likes: 3,
    icons: `<i class="fas fa-utensils fa-5x"  style="color:green;"></i>`,
    img:
      "../images/smart-watch-821557_1280.jpg",
    thumbsUp: '<i class="fas fa-thumbs-up" style="color:green;"></i>',
    thumbsDown: '<i class="fas fa-thumbs-down" style="color:red;"></i>',
    handShake: `<i class="fas fa-handshake fa-2x"></i>`
  },
  {
    id: 5,
    hrs: "6hrs",
    headerText: "Programming",
    header: "Mosh",
    read :"20min",
    author:'../images/manb.jpg',
    commentParagraph:`Hi, welcome to programming! If you've never learned to program before, you might be wondering what programming actually is. Well, when we write a program, we're giving the computer a series of commands that kind of look like a weird form of English. You can think of a computer as a very obedient dog, listening to your every command, and doing whatever you tell it to do. So what's so cool about programming? Well, it really depends on what you think is cool. Because as it turns out, you can use programming for almost everything. Programs control robots that can take care of patients, and my favorite, robots that can roam around Mars and look for water on the surface. Programs help self-driving cars know which way to turn-- which is pretty important! Programs help doctors cure diseases by processing huge amounts of medical data. Programs can be really fun games, like Doodle Jump, Angry Birds, Minecraft. Programs make it possible for Pixar to put out their aweso`,
    likes: 18,
    icons: `<i class="fas fa-seedling fa-5x" style="color:green;"></i>`,
    img: "../images/code-820275_1280.jpg",
    thumbsUp: '<i class="fas fa-thumbs-up" style="color:green;"></i>',
    thumbsDown: '<i class="fas fa-thumbs-down" style="color:red;"></i>',
    handShake: `<i class="fas fa-handshake fa-2x"></i>`
  },
  {
    id: 6,
    hrs: "2hrs",
    headerText: "Visual Studio Code",
    header: "Maximillan",
    read:"10 min",
    author:'../images/people4.jpg',
    commentParagraph:`Microsoft Visual Studio is an integrated development environment (IDE) from Microsoft. It is used to develop computer programs for Microsoft Windows. Visual Studio is one stop shop for all applications built on the .Net platform. One can develop, debug and run applications using Visual Studio. <br/>

    Both Forms-based and web-based applications can be designed and developed using this IDE. The Visual Studio has the below-mentioned features`,
    likes: 7,
    icons: `<i class="fas fa-shower fa-5x"></i>`,
    img:"../images/computer.jpg",
    thumbsUp: '<i class="fas fa-thumbs-up" style="color:green;"></i>',
    thumbsDown: '<i class="fas fa-thumbs-down" style="color:red;"></i>',
    handShake: `<i class="fas fa-handshake fa-2x"></i>`
  },
  {
    id: 7,
    hrs: "5hrs",
    read :"8min",
    headerText :'Artificial Intelligence',
    author:'../images/woman.jpg',
    header :'AMIT MALEWAR',
    commentParagraph:`
    Robotic and artificial intelligence is improving continuously. Robots are already started to take humans’ job, and it’s going to continue. But precisely which job they’ll grab continues to be the topic of debates.
    <br/>
    A humanoid robot has recently read the news on Russian-24, a state-owned television channel. During a Tuesday afternoon news segment on the channel, the silicone-faced anchor presented some run-of-the-mill updates – and even touched on a lurid case in which local authorities were hunting down a mayoral candidate who allegedly kicked a homeless cat.
    <br/>
    It’s fair to say that the robot still needs some work- Ahh… no, a lot of work.
    <br/>
    Alex was created by the Perm-based startup Promobot, which produces service robots with computer-screen faces for state bank Sberbank. His silicon head is modeled on the face of the company’s co-founder Alexei Yuzhakov.`,
    likes: 5,
    icons: `<i class="fab fa-apple fa-5x" style="color:green;"></i>`,
    img:"../images/alex-knight-2EJCSULRwC8-unsplash.jpg",
    thumbsUp: '<i class="fas fa-thumbs-up" style="color:green;"></i>',
    thumbsDown: '<i class="fas fa-thumbs-down" style="color:red;"></i>',
    handShake: `<i class="fas fa-handshake fa-2x"></i>`
  },
  {
    id: 8,
    hrs: "9hrs",
    header: "Eating Pawpaw",
    read :"17min",

    commentParagraph:
      "Papaya fruits are commonly eaten fresh. The may also be processed into jams, jellies and juices are dried and candied.",
    likes: 4,
    icons: `<i class="fas fa-lemon fa-5x" style="color:orange;"></i>`,
    img:
      "../images/smart-watch-821557_1280.jpg",
    thumbsUp: '<i class="fas fa-thumbs-up" style="color:green;"></i>',
    thumbsDown: '<i class="fas fa-thumbs-down" style="color:red;" ></i>',
    handShake: `<i class="fas fa-handshake fa-2x"></i>`
  },
  {
    id: 9,
    hrs: "7hrs",
    header: "Drinking Water",
    read :"11min",

    commentParagraph:
      "Water helps dissolve minerals and nutrients, making them more accessible to the body",
    likes: 3,
    icons: `<i class="fas fa-utensils fa-5x"  style="color:green;"></i>`,
    img:
      "../images/bar-621033_1920.jpg",
    thumbsUp: '<i class="fas fa-thumbs-up" style="color:green;"></i>',
    thumbsDown: '<i class="fas fa-thumbs-down" style="color:red;"></i>',
    handShake: `<i class="fas fa-handshake fa-2x"></i>`
  },
  {
    id: 10,
    hrs: "6hrs",
    header: "Seedling",
    read :"14min",

    commentParagraph:
      "Water helps dissolve minerals and nutrients, making them more accessible to the body",
    likes: 18,
    icons: `<i class="fas fa-seedling fa-5x" style="color:green;"></i>`,
    img:"../images/alex-knight-2EJCSULRwC8-unsplash.jpg",
    thumbsUp: '<i class="fas fa-thumbs-up" style="color:green;"></i>',
    thumbsDown: '<i class="fas fa-thumbs-down" style="color:red;"></i>',
    handShake: `<i class="fas fa-handshake fa-2x"></i>`
  },
  {
    id: 11,
    hrs: "2hrs",
    read :"16min",

    header: "Drinking Water.",
    commentParagraph:
      "Water helps dissolve minerals and nutrients, making them more accessible to the body",
    likes: 7,
    icons: `<i class="fas fa-shower fa-5x"></i>`,
    img:
    "../images/smart-watch-821557_1280.jpg",
    thumbsUp: '<i class="fas fa-thumbs-up" style="color:green;"></i>',
    thumbsDown: '<i class="fas fa-thumbs-down" style="color:red;"></i>',
    handShake: `<i class="fas fa-handshake fa-2x"></i>`
  },
  {
    id: 12,
    hrs: "5hrs",
    header: "Eating Fruid",
    read :"18min",

    commentParagraph:
      "Fruit is healthy for most people. While excessive sugar intake can be harmful, this doesn't apply to whole fruits.",
    likes: 5,
    icons: `<i class="fab fa-apple fa-5x" style="color:green;"></i>`,
    img:
    "../images/smart-watch-821557_1280.jpg",
    thumbsUp: '<i class="fas fa-thumbs-up" style="color:green;"></i>',
    thumbsDown: '<i class="fas fa-thumbs-down" style="color:red;"></i>',
    handShake: `<i class="fas fa-handshake fa-2x"></i>`
  },
  {
    id: 13,
    hrs: "9hrs",
    header: "Eating Pawpaw",
    read :"1min",

    commentParagraph:
      "Papaya fruits are commonly eaten fresh. The may also be processed into jams, jellies and juices are dried and candied.",
    likes: 4,
    icons: `<i class="fas fa-lemon fa-5x" style="color:orange;"></i>`,
    img:
      "../images/imac-1999636_1280.png",
    thumbsUp: '<i class="fas fa-thumbs-up" style="color:green;"></i>',
    thumbsDown: '<i class="fas fa-thumbs-down" style="color:red;" ></i>',
    handShake: `<i class="fas fa-handshake fa-2x"></i>`
  },
  {
    id: 14,
    hrs: "7hrs",
    read :"11min",

    header: "Drinking Water",
    commentParagraph:
      "Water helps dissolve minerals and nutrients, making them more accessible to the body",
    likes: 3,
    icons: `<i class="fas fa-utensils fa-5x"  style="color:green;"></i>`,
    img:
      "https://cdn.pixabay.com/photo/2017/02/20/10/57/model-2082146_1280.jpg",
    thumbsUp: '<i class="fas fa-thumbs-up" style="color:green;"></i>',
    thumbsDown: '<i class="fas fa-thumbs-down" style="color:red;"></i>',
    handShake: `<i class="fas fa-handshake fa-2x"></i>`
  },
  {
    id: 15,
    hrs: "6hrs",
    header: "Seedling",
    read :"19min",

    commentParagraph:
      "Water helps dissolve minerals and nutrients, making them more accessible to the body",
    likes: 18,
    icons: `<i class="fas fa-seedling fa-5x" style="color:green;"></i>`,
    img: "https://cdn.pixabay.com/photo/2015/09/02/12/39/woman-918583_1280.jpg",
    thumbsUp: '<i class="fas fa-thumbs-up" style="color:green;"></i>',
    thumbsDown: '<i class="fas fa-thumbs-down" style="color:red;"></i>',
    handShake: `<i class="fas fa-handshake fa-2x"></i>`
  },
  {
    id: 16,
    hrs: "2hrs",
    header: "Drinking Water.",
    read :"7min",

    commentParagraph:
      "Water helps dissolve minerals and nutrients, making them more accessible to the body",
    likes: 7,
    icons: `<i class="fas fa-shower fa-5x"></i>`,
    img:`../images/hitesh-choudhary-pMnw5BSZYsA-unsplash.jpg`,
    thumbsUp: '<i class="fas fa-thumbs-up" style="color:green;"></i>',
    thumbsDown: '<i class="fas fa-thumbs-down" style="color:red;"></i>',
    handShake: `<i class="fas fa-handshake fa-2x"></i>`
  },
  {
    id: 17,
    hrs: "5hrs",
    header: "Eating Fruid",
    read :"2min",

    commentParagraph:
      "Fruit is healthy for most people. While excessive sugar intake can be harmful, this doesn't apply to whole fruits.",
    likes: 5,
    icons: `<i class="fab fa-apple fa-5x" style="color:green;"></i>`,
    img:"../images/alex-knight-2EJCSULRwC8-unsplash.jpg",
    thumbsUp: '<i class="fas fa-thumbs-up" style="color:green;"></i>',
    thumbsDown: '<i class="fas fa-thumbs-down" style="color:red;"></i>',
    handShake: `<i class="fas fa-handshake fa-2x"></i>`
  },
  {
    id: 18,
    hrs: "9hrs",
    header: "Eating Pawpaw",
    read :"8min",

    commentParagraph:
      "Papaya fruits are commonly eaten fresh. The may also be processed into jams, jellies and juices are dried and candied.",
    likes: 4,
    icons: `<i class="fas fa-lemon fa-5x" style="color:orange;"></i>`,
    img:
    "../images/imac-1999636_1280.png",
    thumbsUp: '<i class="fas fa-thumbs-up" style="color:green;"></i>',
    thumbsDown: '<i class="fas fa-thumbs-down" style="color:red;" ></i>',
    handShake: `<i class="fas fa-handshake fa-2x"></i>`
  },
  {
    id: 19,
    hrs: "7hrs",
    header: "Drinking Water",
    read :"1min",
    author:'../images/java.png',
    headerText:'Programming With Java',
    header:'Herculan Peace',
    commentParagraph:`Java is a general-purpose programming language that is class-based, object-oriented, and designed to have as few implementation dependencies as possible. It is intended to let application developers write once, run anywhere (WORA), meaning that compiled Java code can run on all platforms that support Java without the need for recompilation.<br/> Java applications are typically compiled to bytecode that can run on any Java virtual machine (JVM) regardless of the underlying computer architecture. The syntax of Java is similar to C and C++, but it has fewer low-level facilities than either of them. As of 2019, Java was one of the most popular programming languages in use according to GitHub, particularly for client-server web applications, with a reported 9 million developers.`,
    likes: 3,
    icons: `<i class="fas fa-utensils fa-5x"  style="color:green;"></i>`,
    img:`../images/hitesh-choudhary-pMnw5BSZYsA-unsplash.jpg`,
    thumbsUp: '<i class="fas fa-thumbs-up" style="color:green;"></i>',
    thumbsDown: '<i class="fas fa-thumbs-down" style="color:red;"></i>',
    handShake: `<i class="fas fa-handshake fa-2x"></i>`
  },
  {
    id: 20,
    hrs: "6hrs",
    header: "Joel Maxsum",
    read:"10 min",
    headerText:'Hypertext Markup Language',
    commentParagraph:
      "Water helps dissolve minerals and nutrients, making them more accessible to the body",
    likes: 18,
    icons: `<i class="fas fa-seedling fa-5x" style="color:green;"></i>`,
    img: "../images/html.jpg",
    thumbsUp: '<i class="fas fa-thumbs-up" style="color:green;"></i>',
    thumbsDown: '<i class="fas fa-thumbs-down" style="color:red;"></i>',
    handShake: `<i class="fas fa-handshake fa-2x"></i>`
  }
];

