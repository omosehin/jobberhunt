


function bigImg(x) {
    x.classList = 'fas fa-arrow-left py-4'
    document.querySelector('.goLeft').style.display = 'none'

}

function normalImg(x) {
    x.classList = 'fas fa-pencil-alt py-4'
    document.querySelector('.goLeft').style.display = 'none'

}

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};
let post_id = getUrlParameter('postId');

fetchPost()
function fetchPost(id = post_id) {
    fetch(`${BaseUrl}posts/${id}`)
        .then((res) => res.json())
        .then(data => SinglePost(data))
        .catch(err => {
        //   document.querySelector('.blogError').innerHTML = 'Fail to fetch';
        });
}



fetchComment()
function fetchComment(id = post_id) {
    fetch(`${BaseUrl}comments?postId=${id}`)
        .then((res) => res.json())
        .then(data => postComments(data))
        .catch(err => {
        //   document.querySelector('.blogError').innerHTML = 'Fail to fetch';
        });
}




function SinglePost(data){
    const markup = `
        <div class = "singlePost1 ">
            <h2 class = "latestPostHeading">Latest Business Post</h2>
            <h2 class="PostTtitle">${CapitaliseFirstLetter(data.title)}</h2>
            <p>${CapitaliseFirstLetter(data.body)}</p>
        </div>
    `
    document.querySelector('.singlePost').innerHTML = markup;

}


function deleteCommet(id) {
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
        method: 'DELETE'
    })
    .then(res =>res.json())
    .then(data=>console.log(data))
    .catch(err=>console.log(err))
}



function postComments(data){
    let j = 1; 
    for(let i in data){
        document.querySelector('.postRow').innerHTML += 
        `<tr id = "mytableRow">   
        <td>${j++}</td> 
        <td>${CapitaliseFirstLetter(data[i].name)}</td>
        <td>${CapitaliseFirstLetter(data[i].body)}</td>
        <td class = "dataEmail">${CapitaliseFirstLetter(data[i].email)}</td>
        <td> 
        <a href="" class="delete" onClick = '${deleteCommet(data[i].id)}'>
        <i class = 'fas fa-trash-alt float-right'></i></a>
        </td>
    </tr> `
    };
    
}


function myCommentSearch() {
    let searchComment = document.getElementById('searchComment').value.toUpperCase();
    let ji = document.querySelectorAll('.dataEmail');
    for (let j = 0; j < ji.length; j++) {
       if((ji[j].innerHTML.toUpperCase()).includes(searchComment)){
           ji[j].parentNode.style.display = '';
       }else{
        ji[j].parentNode.style.display = 'none';
  
       }
        
    }

}

