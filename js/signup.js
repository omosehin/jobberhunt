let canSignUp = true;
const SignUpForm = document.forms['signUpForm'];
let emailError = document.getElementById("email_error");
let passwordError = document.getElementById("password_error");
let comfirmPasswordError = document.getElementById("comfirmPassword_error");
var email = SignUpForm.email;
var password = SignUpForm.password;
var comfirm_password = SignUpForm.Comfirm_password;

 email.addEventListener("blur",emailVerify,true);
 password.addEventListener("blur",passwordVerify,true);

SignUpForm.addEventListener('submit', signUpForm1, false);

function emailVerify() {
  if(email.value !=""){
    document.querySelector('.emailStyle').style.border = "white";
    emailError.innerHTML = "";
    return true;
  }
}
function passwordVerify() {
  if(password.value !=""){
    document.querySelector('.passwordStyle').style.border = "white";
    passwordError.innerHTML = "";
    return true;
  }
}


function signUpForm1(event) {
  event.preventDefault();
  let data = {};
  let signUp = [];
  data.email = SignUpForm.email.value;
  data.password = SignUpForm.password.value;
  data.password = SignUpForm.Comfirm_password.value;
  
  if(email.value ==""){
     document.querySelector('.emailStyle').style.border = "1px solid red";
    emailError.textContent = "Email is required";
    email.focus();
    return false;
  }
  if(!/\S+@\S+\.\S+/.test(email.value)){
    document.querySelector('.emailStyle').style.border = "1px solid red";
   emailError.textContent = "Valid Email is required";
   email.focus();
   return false;
 }
  
  if(password.value ==""){
    document.querySelector('.passwordStyle').style.border = "1px solid red";
    passwordError.textContent = "Password is required";
    password.focus();
    return false;
  }
  if(comfirm_password.value ==""){
    document.querySelector('.passwordStyle').style.border = "1px solid red";
    comfirmPasswordError.textContent = "Password 2 is required";
    comfirm_password.focus();
    return false;
  }
  if(password.value != comfirm_password.value){
    document.querySelector('.passwordStyle').style.border = "1px solid red";
    document.querySelector('.passwordStyle2').style.border = "1px solid red";
    comfirmPasswordError.textContent = "The two Password do not match";
    comfirm_password.focus();
    return false;
  }

  

  validateSignUp(data);
  if (localStorage.getItem("Signup") !== null && canSignUp == true) {
    let getItem = JSON.parse(localStorage.getItem('Signup'));
    getItem.push(data);
    storeItem(JSON.stringify(getItem));
  
  }
  else if (localStorage.getItem("Signup") == null && canSignUp == true) {
    signUp.push(data)
    storeItem(JSON.stringify(signUp));
  }

  return data;
}


function storeItem(dataset) {
  localStorage.setItem('Signup', dataset)
}

function validateSignUp(Data) {
  let signUpError = document.getElementById('signUpError')
  let successSignUpMsg = document.querySelector('.successSignUpMsg')
  let allRegisterUser = JSON.parse(localStorage.getItem('Signup'));
  if (localStorage.getItem("Signup") !== null) {
    for (const Obj of allRegisterUser) {
      if (Object.values(Obj).includes(Data.email)) {
        signUpError.style.display = 'block';

        successSignUpMsg.style.display = 'none';
        canSignUp = false;

        return;
      }
      else{
        signUpError.style.display = 'none';
        successSignUpMsg.style.display = 'block';
        gotoLandingPage();
        canSignUp = true;
      }

    }
  }else{
    successSignUpMsg.style.display = 'block';
    gotoLandingPage();

  }

}

function gotoLandingPage(){
  setTimeout(() => {
    window.location.href ='index.html';
  }, 2000);
}