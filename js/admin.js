//window.localStorage.removeItem('createJOB');
// let textareaCKeditor = document.getElementById('des');
// CKEDITOR.replace('textareaCKeditor');

const jobForm = document.forms['createForm'];
jobForm.addEventListener('submit', createJobFunction, false);
let create = false;
let savedLoginForm = localStorage.getItem('login');

let createAdmin = localStorage.setItem('adminEmail', "omosehin14@gmail.com")
noJob()
function noJob() {
  if (localStorage.getItem("createJOB") == null){
    document.querySelector('.noJob').style.display = 'block'
  }
  
}



function createNode(element) {
  return document.createElement(element);
}



function append(parent, el) {
  return parent.appendChild(el);
}
function emptyFormsAfterSubmit() {
  jobForm.title.value = '';
  jobForm.company.value='';
  jobForm.jobtype.value='';
  jobForm.location.value='';
  jobForm.des.value = '';
  jobForm.endDate.value = '';
  jobForm.startDate.value ='';
}
function createJobFunction(event) {
  event.preventDefault();
  document.querySelector('.noJob').innerHTML = ''
   document.querySelector('ul').innerHTML = '';
  let data = {};
  let allJobsCreated = [];
  data.title = jobForm.title.value;
  data.company = jobForm.company.value;
  data.location = jobForm.location.value;
  data.type = jobForm.jobtype.value;
  data.startdate = jobForm.startDate.value;
  data.enddate = jobForm.endDate.value;
  data.description = jobForm.des.value;
  data.id = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
  data.roles = 'user';

  if (localStorage.getItem("createJOB") !== null) {
    let getJob = JSON.parse(localStorage.getItem('createJOB'));
    getJob.push(data);
    storeJobs(JSON.stringify(getJob));
    emptyFormsAfterSubmit();
  }
  else if (localStorage.getItem("createJOB") == null) {
    allJobsCreated.push(data);
    storeJobs(JSON.stringify(allJobsCreated));
    emptyFormsAfterSubmit();
  }
  getJobCreated(JSON.parse(localStorage.getItem('createJOB')))
  
}



function storeJobs(dataset) {
  localStorage.setItem('createJOB', dataset)
}

function getJobCreated(getJob) {
  if(getJob){for (let i = getJob.length - 1; i >= 0; i--) {
    createDomforJobs(getJob[i]);
  }
}else return;
}

function deleteJob(id) {
  document.querySelector('ul').innerHTML = '';
  let allJobs = JSON.parse(localStorage.getItem('createJOB'));
  const filterJob = allJobs.filter( job=> job.id !== id)
  storeJobs(JSON.stringify(filterJob));
  getJobCreated(JSON.parse(localStorage.getItem('createJOB')))
}

function createDomforJobs(data) {
    let li = createNode("li"),
    listDiv = createNode("div"),
    titleParagraph = createNode("p"),
    titleKey = createNode("h3"),
    company = createNode("p"),
    companyKey = createNode("h3"),
    viewButton = createNode("Button"),

    jobtype = createNode("p"),
    delIcon = createNode("i"),
    typeKey = createNode("h3"),
    id = createNode("p"),

    location = createNode("h3"),
    locationKey = createNode("p"),

    descriptionKey = createNode("h3"),
    description = createNode("p"),
    startDate = createNode("p");
  description = createNode("p"),
    startDateKey = createNode("h3"),
    endDate = createNode("p");
  endDateKey = createNode("h3");

  titleKey.classList = 'JobTitle';
  companyKey.classList = 'JobTitle';
  descriptionKey.classList = 'JobTitle';
  startDateKey.classList = 'JobTitle';
  endDateKey.classList = 'JobTitle';
  typeKey.classList = "JobTitle";
  location.classList = "JobTitle";


  titleParagraph.textContent = data.title;
  titleKey.textContent = 'Job Title';
  typeKey.textContent = 'Job Type';
  viewButton.textContent = 'view Details'
  location.textContent = 'Job Location';
  descriptionKey.textContent = 'Job Description';
  companyKey.textContent = 'Company';
  startDateKey.textContent = 'Start Date';
  endDateKey.textContent = 'Closing Date';
  jobtype.textContent = data.type;

  endDateKey.classList = 'JobTitle';
  company.textContent = data.company;
  locationKey.textContent = data.location;
  description.textContent = data.description.split(" ").splice(0,12).join(" ") + '...';
  startDate.textContent = data.startdate;
  endDate.textContent = data.enddate;
  id.textContent = data.id;
  description.classList = 'desClass';
  startDate.classList = 'desClass';
  endDate.classList = 'desClass';
  company.classList = 'desClass';
  titleParagraph.classList = 'desClass';
  jobtype.classList = "desClass";
  locationKey.classList = "desClass";
  delIcon.classList = 'fas fa-trash-alt fa-2x float-right'
  delIcon.value =data.id;
  li.classList = "listU";
  id.classList = "id";
  viewButton.classList = 'view btn btn-outline-primary';
  titleParagraph.style.fontWeight = 'bolder';
  viewButton.value = data.id;
  append(li, id);
  append(li, titleKey);
  append(li, titleParagraph);
  append(li, companyKey);
  append(li, company);
  append(li, location);
  append(li, locationKey);
  append(li, typeKey);
  append(li, jobtype);
  append(li, descriptionKey);
  append(li, delIcon);

  append(li, description);

  append(li, startDateKey);
  append(li, startDate);
  append(li, endDateKey);
  append(li, endDate);
  append(li, viewButton);

  document.querySelector('ul').append(li);
}


document.querySelector('.list').addEventListener('click', (event) => {
  let detailsJobId = event.target.value;
  localStorage.setItem('jobID', detailsJobId);
  
  if (localStorage.getItem('jobID') !== null && event.target.value && event.target.textContent == 'view Details') {
    window.location.href = "viewDetails.html";
  }
});

document.querySelector('.list').addEventListener('click', (event) => {
  if (localStorage.getItem('jobID') !== null && event.target.textContent !== 'view Details') {
   deleteJob(event.target.value)
  }
});

// window.addEventListener('DOMContentLoaded', getJobCreated(JSON.parse(localStorage.getItem('createJOB'))));
 getJobCreated(JSON.parse(localStorage.getItem('createJOB')))
