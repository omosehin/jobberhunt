//Global controller

//Blog Controller

//UI Controller

const display = document.querySelector("#data");
const display2 = document.querySelector("#singlePost");
const display3 = document.querySelector("#commentSection");

function getBlogs() {
  fetch("https://jsonplaceholder.typicode.com/posts")
    .then(response => response.json())
    .then(json => {
      console.log(json);
      (function() {
        document.getElementById("first").addEventListener("click", firstPage);
        document.getElementById("next").addEventListener("click", nextPage);
        document
          .getElementById("previous")
          .addEventListener("click", previousPage);
        document.getElementById("last").addEventListener("click", lastPage);

        let list = json;
        let pageList = [];
        let currentPage = 1;
        let numberPerPage = 10;
        let numberOfPages = 0;

        function makeList() {
          numberOfPages = getNumberOfPages();
        }

        function getNumberOfPages() {
          return Math.ceil(list.length / numberPerPage);
        }

        function nextPage() {
          currentPage += 1;
          loadList();
          document.documentElement.scrollTop = 0;
        }

        function previousPage() {
          currentPage -= 1;
          loadList();
          document.documentElement.scrollTop = 0;
        }

        function firstPage() {
          currentPage = 1;
          loadList();
          document.documentElement.scrollTop = 0;
        }

        function lastPage() {
          currentPage = numberOfPages;
          loadList();
          document.documentElement.scrollTop = 0;
        }

        function loadList() {
          var begin = (currentPage - 1) * numberPerPage;
          var end = begin + numberPerPage;

          pageList = list.slice(begin, end);
          console.log(pageList);
          drawList();
          check();
        }

        function drawList() {
          display.innerHTML = "";
          pageList.forEach(el => {
            display.innerHTML += `<div class="single-post"><div class="title" id="main-title"><div class="blog-content"><h4>${el.title}</h4><p>${el.body}</p><p>by <span class="lorem">Lorem ipsum</span></p><p>October 9, 2019</p></p><a href="viewBlog.html?id=${el.id}" class="view">View Post</a></div><div id="image">	<img src="images/adult-businessman-close-up-374820.jpg" id="indexImage" alt=""></div></div></div>`;
          });
        }

        function check() {
          document.getElementById("next").disabled =
            currentPage == numberOfPages ? true : false;
          document.getElementById("previous").disabled =
            currentPage == 1 ? true : false;
          document.getElementById("first").disabled =
            currentPage == 1 ? true : false;
          document.getElementById("last").disabled =
            currentPage == numberOfPages ? true : false;
        }

        function load() {
          makeList();
          loadList();
        }

        window.onload = load();
      })();
    });
}

getBlogs();

