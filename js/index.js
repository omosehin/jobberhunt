// localStorage.clear();

//window.localStorage.removeItem('createJOB');
let allRegisterUser = JSON.parse(localStorage.getItem('Signup'));


const search = document.forms['searchF'];

search.addEventListener('click', myInputChange)

search.searchInput.value = ''
function myInputChange() {
  if (search.searchInput.value == null ) {
    document.getElementById('loginNavbtn').style.disabled = "true";
   return;
  }
  else{
    window.location.href = 'client.html';
  }
}

document.querySelector('.createAcc').addEventListener('click', () => {
  window.location.href = 'signup.html';
})


let allJob = JSON.parse(localStorage.getItem("createJOB"));
homeviewDetailsFunc();

let LoginButton = document.getElementById('loginNavButton')
let loginNavbtn = document.getElementById('loginNavbtn')
const form = document.forms['signInForm'];
form.addEventListener('submit', SignIn, false);

let validate = false;
let savedLoginForm = JSON.parse(localStorage.getItem('login'));



//sign in

var email = form.signInEmail;
email.addEventListener("blur",emailVerify,true);

function emailVerify() {
  if(email.value !=""){
    document.querySelector('.emailStyle').style.border = "1px solid green";
    document.querySelector('#emailError').innerHTML = "";
    return true;
  }
}

function SignIn(event) {
  event.preventDefault();
  const data = {};
  data.Email = form.signInEmail.value;
  data.password = form.signInPassword.value;
  data.roles = 'user';
  if (email.value == '') {
    document.querySelector('.emailStyle').style.border = "1px solid red";
    document.querySelector('#emailError').innerHTML = 'Email is required';
    email.focus();
    return false;
  }
  if(!/\S+@\S+\.\S+/.test(email.value)){
    document.querySelector('.emailStyle').style.border = "1px solid red";
    document.querySelector('#emailError').textContent = 'Valid Email is required';
   email.focus();
   return false;
 }
  else {
    document.querySelector('#emailError').style.display = 'none';
  }
  if (form.signInPassword.value == '') {
    document.querySelector('#paswordError').style.display = 'block';
    return false;
  }
  else {
    document.querySelector('#paswordError').style.display = 'none';
  }
  let emailError = document.getElementById('loginError')
  if ((localStorage.getItem('Signup')) == null) {
    emailError.style.display = 'block';
    return;
  }
  for (const Obj of allRegisterUser) {
    if((Object.values(Obj).includes(data.Email))){
      sessionStorage.setItem('login', JSON.stringify(data)); 
    }else{
      setTimeout(() => {
        emailError.style.display = 'block';
      }, 2000);

    }
  }
  validateLogin(data);
}




function validateLogin(Data) {
  let emailError = document.getElementById('loginError')
  if ((localStorage.getItem('Signup')) !== null) {
    for (const Obj of allRegisterUser) {
      if (Data.Email == "omosehin14@gmail.com") {
        window.location.href = "admin.html";
      }
      if (Data.Email != Obj.email && form.signInEmail.value && form.signInPassword.value) {
        setTimeout(() => {
          emailError.style.display = 'block';
        }, 2000);
      }

      if (Object.values(Obj).includes(Data.Email) && Data.Email != "omosehin14@gmail.com") {
        window.location.href = "client.html";
      }
    }
  }

}

$('#exampleModal').on('hidden.bs.modal', function () {
  let emailError = document.getElementById('loginError')
  emailError.style.display = 'none';
  form.signInEmail.value = '';
  form.signInPassword.value = '';
  document.querySelector('#emailError').style.display = 'none';
  document.querySelector('#paswordError').style.display = 'none';
  document.querySelector('.createAcc').style.color = 'black';
  document.querySelector('.createAcc').style.fontWeight = '';
})




function storeItem(dataset) {
  localStorage.setItem('Signup', dataset)
}
noJob()
function noJob() {
  if (localStorage.getItem("createJOB") !== null) {
    document.querySelector('.noJobs').style.display = 'none';
    document.querySelector('.browseJob').style.display = 'block';

  } else {
    document.querySelector('.noJobs').style.display = 'block';
    document.querySelector('.browseJob').style.display = 'none';
  }
}

function getAvailableJobs() {
  if (allJob) {
    let noOfDataToDisplay = allJob.slice(-3);
    const markup = `
<ul class="items list-unstyled">
    ${noOfDataToDisplay.map(item => `<li>
    <div class='card'>
      <div class="card-body">
        <div class = "d-flex justify-content-between align-items-center">
        <h2 class = "titleIndex">${item.title}</h2>
        <span class="badge badge-success badge-pill p-2">${item.type}</span>
        </div>
        <div class = "d-flex justify-content-between align-items-center py-3">
        <h2 class="text-muted jobdescription">Job Description</h2>
        <span class="font-weight-bolder">${item.location}</span>
        </div>
        <div class = "d-flex justify-content-between align-items-center">
        <p class ="">${item.description.split(" ").splice(0, 12).join(" ") + '...'}</p> 
        <button class="btn font-weight-bold" type = "button" value = ${item.id} >${'View Detail'} <i class="fas fa-arrow-right pl-2"></i></button>
        </div>
     
      </div>
    </div>

   
    </li>`)}
</ul>
`;
    document.querySelector('.jobOpening_1').innerHTML = markup;

  } else return
}



function homeviewDetailsFunc() {

  document.querySelector('.jobOpening_1').addEventListener('click', (e) => {
    let detailsJobId = e.target.value;
    if (e.target.value) {
      localStorage.setItem('Jobdetails', detailsJobId);
      window.location.href = 'HomeJobDetails.html';
    }
    event.stopPropagation();
  }, false);
}

getAvailableJobs();

displayHomeJobIndex();

function displayHomeJobIndex() {
  if (allJob) {
    let displayAvailableJobDescription = `
  <div class="font-weight-bolder">
  <span class=${allJob ? 'allJobLength ' : ''}>${allJob ? allJob.length : ''}</span>
  <span class ='text-muted allJobLength'>role${allJob.length > 1 ? 's' : ''} in 4 Locations</span>
  </div>
  `;
    document.querySelector('.rolesLength').innerHTML = displayAvailableJobDescription;
  } else return;
}
