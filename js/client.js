const searchClientForm = document.forms['searchClientForm'];
//window.localStorage.removeItem('createJOB');
noJob()
searchClientForm.addEventListener('submit', searchFunctiona, false);
let locationInput, typeInput, titleInput;

function searchFunctiona(event) {
  event.preventDefault();
  document.querySelector('ul').innerHTML = '';
  let searchData = {};
  searchData.location = searchClientForm.location.value;
  searchData.type = searchClientForm.jobtype.value;
  searchData.title = searchClientForm.title.value;
  searchFunction(searchData)

  return searchData
}

let loginUser = JSON.parse(sessionStorage.getItem('login'));

const markup = `${loginUser ? loginUser.Email : 'No user Login In'}`;
document.querySelector('.loginPerson').innerHTML = markup;



let AllJobs = JSON.parse(localStorage.getItem('createJOB'));

// let openings = document.querySelector('.openings')
let textSpace = document.querySelector('.updateJobs')
let getJob = AllJobs && AllJobs.length >= 1 ? AllJobs.length + " Jobs Online now" : "Check for Available Jobs Later";
let jobTextNode = document.createTextNode(getJob);
textSpace.appendChild(jobTextNode)
// openings.appendChild(jobTextNode)


function createNode(element) {
  return document.createElement(element);
}

function append(parent, el) {
  return parent.appendChild(el);
}

function storeJobs(dataset) {
  localStorage.setItem('createJOB', dataset)
}

noJob()
function noJob() {
  if (localStorage.getItem("createJOB") !== null) {
    document.querySelector('.noJobs').style.display = 'none';

  } else {
    document.querySelector('.noJobs').style.display = 'block';
    document.getElementById('titleSelect').style.display = 'none';
  }
}

function searchFunction(search) {
  const { location, type, title } = search;
  let allsearch = location.trim().toLowerCase() || type.trim().toLowerCase() || title.trim().toLowerCase();
  let data = JSON.parse(localStorage.getItem('createJOB'));

  let filteredData = [];
  if (allsearch.length > 0) {
    filteredData = data.filter(function (obj) {
      return Object.keys(obj).some(function (key) {
        return obj[key].toLowerCase().includes(allsearch);
      });
    });
  }
  getJobCreated2(filteredData)
  // return data;
}


function getJobCreated2(arrayData) {
  if ((localStorage.getItem('createJOB') !== null)) {
    for (let i = arrayData.length - 1; i >= 0; i--) {
      createDomforJobs(arrayData[i]);
    }
  } else return;
}


function createDomforJobs(data) {

  let li = createNode("li"),
    listDiv = createNode("div"),
    titleParagraph = createNode("p"),
    titleKey = createNode("h3"),
    viewButton = createNode("Button"),

    jobtype = createNode("p"),
    typeKey = createNode("h3"),

    location = createNode("h3"),
    locationKey = createNode("p"),


    descriptionKey = createNode("h3"),
    description = createNode("p"),
    startDate = createNode("p");
  description = createNode("p"),
    startDateKey = createNode("h3"),
    endDate = createNode("p");
  endDateKey = createNode("h3"),

    titleKey.classList = 'JobTitle';
  descriptionKey.classList = 'JobTitle';
  startDateKey.classList = 'JobTitle';
  endDateKey.classList = 'JobTitle';
  typeKey.classList = "JobTitle";
  location.classList = "JobTitle";

  titleParagraph.textContent = data.title;
  titleKey.textContent = 'Job Title';
  typeKey.textContent = 'Job Type';
  location.textContent = 'Job Location';
  viewButton.textContent = 'view Details'

  descriptionKey.textContent = 'Job Description';
  startDateKey.textContent = 'Start Date';
  endDateKey.textContent = 'Closing Date';
  jobtype.textContent = data.type;

  endDateKey.classList = 'JobTitle';

  locationKey.textContent = data.location;
  description.textContent = data.description.split(" ").splice(0, 6).join(" ");
  startDate.textContent = data.startdate;
  endDate.textContent = data.enddate;

  description.classList = 'desClass';
  startDate.classList = 'desClass';
  endDate.classList = 'desClass';
  titleParagraph.classList = 'desClass';
  jobtype.classList = "desClass";
  locationKey.classList = "desClass";
  viewButton.classList = 'view btn btn-outline-primary';
  viewButton.value = data.id;


  append(li, titleKey);
  append(li, titleParagraph);
  append(li, location);
  append(li, locationKey);
  append(li, typeKey);
  append(li, jobtype);
  append(li, descriptionKey);

  append(li, description);

  append(li, startDateKey);
  append(li, startDate);
  append(li, endDateKey);
  append(li, endDate);
  append(li, viewButton);

  document.querySelector('ul').append(li);
  //  document.getElementsByTagName('li').add('list-group-item')
}


document.querySelector('.list_all').addEventListener('click', (event) => {
  let detailsJobId = event.target.value;
  localStorage.setItem('jobID', detailsJobId);

  if (localStorage.getItem('jobID') !== null && event.target.value) {
    window.location.href = "viewDetails.html";
  }
});


document.querySelector('.logout').addEventListener('click',function (e){
  e.preventDefault();
  if(sessionStorage.getItem('login') !==null){
      sessionStorage.removeItem('login');
      window.location.href = '../index.html';
  }else{
      window.location.href = '../index.html';
  }
});


getJobCreated2(JSON.parse(localStorage.getItem('createJOB')))


uniqueTitle(AllJobs, 'title');

function uniqueTitle(arr, comp) {

  if ((localStorage.getItem('createJOB') !== null)) {

    const unique = arr
      .map(e => e[comp])

      // store the keys of the unique objects
      .map((e, i, final) => final.indexOf(e) === i && i)

      // eliminate the dead keys & store unique objects
      .filter(e => arr[e]).map(e => arr[e]);
    filterTitle(unique)
  } else return
}
//capitalise first letter in a title
function filterTitle(unique) {
  const selectElement = ` 
        <select class="form-control" name="jobtype" id="jobtype" required>
        <option disabled selected value="">Please select Job Title</option>
         ${unique.map(i => `<option>${i.title.toLowerCase().split(' ').map(w => w.substring(0, 1).toUpperCase() + w.substring(1)).join(' ')}</option>`)}
         </select>`;
  document.getElementById('titleSelect').innerHTML = selectElement;
}



uniqueLocation(AllJobs, 'location');

function uniqueLocation(arr, comp) {

  if ((localStorage.getItem('createJOB') !== null)) {

    const uniqueLocation = arr
      .map(e => e[comp])

      // store the keys of the unique objects
      .map((e, i, final) => final.indexOf(e) === i && i)

      // eliminate the dead keys & store unique objects
      .filter(e => arr[e]).map(e => arr[e]);
    filterLocation(uniqueLocation)
  } else return
}
//capitalise first letter in a title
function filterLocation(uniqueLocation) {
  const selectElement = ` 
        <select class="form-control" name="location" id="location" required>
        <option disabled selected value="">Please select Location</option>
         ${uniqueLocation.map(i => `<option>${i.location}</option>`)}
         </select>`;
  document.getElementById('jobLocations').innerHTML = selectElement;
}




uniqueTypes(AllJobs, 'location');

function uniqueTypes(arr, comp) {

  if ((localStorage.getItem('createJOB') !== null)) {

    const uniqueTypes = arr
      .map(e => e[comp])

      // store the keys of the unique objects
      .map((e, i, final) => final.indexOf(e) === i && i)

      // eliminate the dead keys & store unique objects
      .filter(e => arr[e]).map(e => arr[e]);
    filterTypes(uniqueTypes)
  } else {
    document.querySelector('.searchContainer').style.display ='none';
    document.querySelector('.jobAvailable').style.display ='none';}
}
//capitalise first letter in a title
function filterTypes(uniqueTypes) {
  const selectElement = ` 
        <select class="form-control" name="jobtype" id="jobtype" required>
        <option disabled selected value="">Please select Job Type</option>
         ${uniqueTypes.map(i => `<option>${i.type}</option>`)}
         </select>`;
  document.getElementById('jobTypes').innerHTML = selectElement;
}







