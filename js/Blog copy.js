var currPage = 0;
var numOfPages = 0;
var color = false;
const commentForm = document.forms["commentForms"];
let titleError = document.getElementById("title_error");
let commentError = document.getElementById("comment_error");

var title = commentForm.Title;
var comment = commentForm.Comment;

title.addEventListener("blur", titleVerify, true);
comment.addEventListener("blur", commentVerify, true);

function titleVerify() {
  if (title.value != "") {
    titleError.innerHTML = "";
    return true;
  }
}
function commentVerify() {
  if (comment.value != "") {
    commentError.innerHTML = "";
    return true;
  }
}

function emptyForm() {
  title.value = "";
  comment.value = "";
  commentError.innerHTML = "";
}

commentForm.addEventListener("submit", commentFunction, false);
function commentFunction(event) {
  event.preventDefault();
  let commentObj = {};
  commentObj.title = commentForm.Title.value;
  commentObj.comment = commentForm.Comment.value;
  if (title.value == "") {
    titleError.textContent = "Title is required";
    title.focus();
    return false;
  }
  if (comment.value == "") {
    commentError.textContent = "Comment is required";
    comment.focus();
    return false;
  }

  fetch(`${BaseUrl}posts`, {
    method: "post",
    headers: {
      Accept: "application/json, text/plain, */*",
      "Content-Type": "application/json"
    },
    body: JSON.stringify(commentObj)
  })
    .then(res => res.json())
    .then(data => {
      console.log(data);
    })
    .catch(err => {
      console.log(err);
    });
  emptyForm();
}

// pages()

fetchBlog();
// var limit = 0;
function fetchBlog() {
  fetch("https://jsonplaceholder.typicode.com/posts")
    .then(res => res.json())
    .then(data => {
      userData(data);
    })
    .catch(err => {
      document.querySelector(".blogError").innerHTML =
        "Fail to fetch:Check Your Internet Connection";
    });
  // pages();
}

/*
let bgColor = 'active'

document.querySelector('.first').addEventListener('click',(e)=>{
  currPage = 0;
  fetchBlog(limit = currPage);
  numOfPages = 1;
  e.target.classList = bgColor;
    
})
document.querySelector('.next').addEventListener('click',(e)=>{
  currPage = currPage + 5;
  // currPage >=95 ? currPage =  currPage -5 :"";
  numOfPages >19 ? '': fetchBlog(limit = currPage) ;
  numOfPages =numOfPages <=20 ? numOfPages + 1 : 20; 
})
document.querySelector('.prev').addEventListener('click',(e)=>{
 currPage <= 5 ? currPage = 0 : currPage = currPage - 6; 
 numOfPages <= 0 ? '' :  fetchBlog(limit = currPage);
  numOfPages = numOfPages > 0 ? numOfPages -1 : 0 ; 
// e.target.classList = bgColor; 
})

document.querySelector('.last').addEventListener('click',(e)=>{
  numOfPages = 20;
  fetchBlog(limit = 95 );
})

*/

// function pages() {
//   let num = numOfPages
//   const markPages = `
//   <span>${num + ' of'}</span>
//   <span>${20} Pages</span>

//   `
//   document.querySelector('.pages').innerHTML = markPages
// }

const userData = params => {
  var postUsers = {};

  params.forEach(user => {
    postUsers[user.postUsersId] = user;
  });

  var res = ExternalArr.map(function(sub) {
    var user = postUsers[sub.postUsersId];
    if (user) {
      for (var key in user) {
        sub[key] = user[key];
      }
    }
    return sub;
  });

  blogpost(res);
};

function blogpost(data) {
  const markup = ` <div class="row py-5" >
        ${data.map(
          (item, index) => `
                <div class ="col-lg-8 mt-4 mx-auto " data-aos="fade-up" data-aos-duration="1000">
                
                <a  href="../postComment.html?postId=${
                  item.id
                }" class="aTagPost">
                <div class = "card myCard"> 
                  
                  <img src =${
                    item.img
                  } alt ="img" data-key='${index}' class = "blog_image"/>
                  
                  <div class = "ml-3">
                  <p class = "Comment_name">${CapitaliseFirstLetter(
                    item.title.substring(0, 15)
                  )}</p>
                  <p class = "post_body">${CapitaliseFirstLetter(
                    item.body.substring(0, 150)
                  )}</p>
                  
                  <span class = "float-right ml-1 font-weight-bolder">Comments</span>
                  <span class = "float-right commenNum font-weight-bolder">5</span>
                  </div>
                    </div> 
                </a>
                <div>
                  <form class = "mt-2 commentForm">
                    <div class = "d-flex">
                       <input type="text" id = "comment" class = "form-control commentInput" placeholder = "Add a comments" />
                       <button type= "submit"> Comment</button>
                    </div>
                  </form>
                </div>
                <div class = "commenting py-4">
                
                  <div class = "mr-4 sideIcons">${item.icons}</div>
                    <div>                    
                    <span class = "commentHeaderP">${item.header}</span>
                    <span class= "font-weight-bold thumbs"><em>${
                      item.hrs
                    }</em></span>
                    <p>${item.commentParagraph}</p>
                    
                    <ul class = "postLikes">
                    <li><a href = "">${"Reply"}</a></li>
                    <li><a href = "">Share</a></li>
                    <li >${item.likes + " likes"} </li>
                    <li class="thumbs">${item.thumbsUp} </li>
                    <li class="thumbs">${item.thumbsDown} </li>
                    </ul>
                    </div>
                </div>
                
                </div>
               
                `
        )}     
                </div>`;
  document.querySelector(".blogRow").innerHTML = markup;
}
