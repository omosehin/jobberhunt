document.onscroll = function() {
    if (window.scrollY >= 300) {
        document.getElementById('socialIcon').style.display='block';
    }else{
        document.getElementById('socialIcon').style.display='none';
  
    }
}

let loginUser = JSON.parse(sessionStorage.getItem('login'));

disabledApplyButton();
function disabledApplyButton() {
    if(sessionStorage.getItem('login') !== null){
        document.querySelector('.loginApply').style.display = 'none';
        document.querySelector('.applyBtn').style.opacity = '1';
    }else{
        document.querySelector('.loginApply').style.display = 'block';
        document.querySelector('.applyBtn').style.opacity = '0';
    } 
}


document.querySelector('.logout').addEventListener('click',function (e){
    e.preventDefault();
    if(sessionStorage.getItem('login') !==null){
        sessionStorage.removeItem('login');
        window.location.href = '../index.html';
    }else{
        window.location.href = '../index.html';
    }
});

let jobid = localStorage.getItem('Jobdetails');
let getAllArray = JSON.parse(localStorage.getItem('createJOB'));

function namen() {
    for (let i = 0; i < getAllArray.length; i++) {
        if (getAllArray[i].id === jobid) {
            displayDataToDom(getAllArray[i]);
            populateEditInput(getAllArray[i])
        };
    }
}
namen()

    
    document.getElementById('email').value =loginUser&& loginUser.Email;
    function populateEditInput(data) {
        document.getElementById('location').value = data.location;   
        document.getElementById('applyDate').value = new Date().getFullYear();   
    }

function displayDataToDom(data) {
    const markup = `
 <div class="person">
    <h2 class="clientHeadText" >
        ${data.title}
    </h2>
 </div> 
`;


    const companyMarkup = `<p>${data.company ?data.company :'No Job Name Supply'}</p>`
    const jobTypeMarkup = `<p>${data.type}</p>`
    const endDateMarkup = `<p>${data.enddate}</p>`
    const descriptionMarkup = `<p>${data.description}</p>`
    const locationMarkup = `<p>${data.location}</p>`
    document.querySelector('.jobTitleDetails').innerHTML = markup;
    document.querySelector('.jobcom').innerHTML = companyMarkup;
    document.querySelector('.jobdec').innerHTML = descriptionMarkup;
    document.querySelector('.jobloc').innerHTML = locationMarkup;
    document.querySelector('.jobendDate').innerHTML = endDateMarkup;
    document.querySelector('.jobType').innerHTML = jobTypeMarkup;
}

// window.addEventListener('load', namen);

const markup = `${loginUser ? loginUser.Email :'No user Login In'}`;
document.querySelector('.loginPerson').innerHTML = markup;

