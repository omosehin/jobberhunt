

/*
open dropdown when hover

$(document).ready(function () {
    $(".dropdown").hover(function () {
      var dropdownMenu = $(this).children(".dropdown-menu");
      if (dropdownMenu.is(":visible")) {
        dropdownMenu.parent().toggleClass("open");
      }
    });
  });
*/
  
  
  
  $('#year').text(new Date().getFullYear());
  
  $('body').scrollspy({ target: '#main-nav' });
  $("#main-nav a").on('click', function (event) {
    if (this.hash !== "") {
      event.preventDefault();
  
      const hash = this.hash;
  
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function () {
  
        window.location.hash = hash;
      });
    }
  });
  
  